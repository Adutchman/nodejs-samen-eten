//
// Authentication controller
//
const assert = require("assert");
const jwt = require("jsonwebtoken");
const logger = require("../config/config").logger;
const jwtSecretKey = require("../config/config").jwtSecretKey;
const bcrypt = require("bcrypt");
const inputChecks = require("../inputChecks.js");
const authentication = require("../services/authenticationService.js");

// The info that is sent back in the info method
const info = {
    student_name: "Stijn van Houwelingen",
    student_number: "2169664",
    description: "A NodeJS server for samen-eten",
    sonarqube_url: "https://sonarqube.avans-informatica-breda.nl/dashboard?id=nodejs-samen-eten-SvH",
};

/**
 * @description Validates new registrations to include all needed params. It logs mistakes
 * @return {boolean} - if the data is valid, it returns true.
 */
function validateRegister(req, next) {
    try {
        assert(
            typeof req.body.firstName === "string",
            "firstName is not a string or missing."
        );
        assert(
            typeof req.body.lastName === "string",
            "lastName is not a string or missing."
        );
        assert(
            typeof req.body.email === "string",
            "email is not a string or missing."
        );
        assert(
            typeof req.body.password === "string",
            "password is not a string or missing."
        );
        assert(
            typeof req.body.studentNumber === "number",
            "studentHome is not a string or missing."
        );
    } catch (error) {
        console.error("User data is invalid: ", error.message);
        next({ message: error.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        logger.debug("Some inputcheck failed, see returned response");
        next({ message: returnMessage, status: 400 });
    }

    if (
        inputChecks.emailCheck(req.body.email, errorCallback) &&
        inputChecks.passwordCheck(req.body.password, errorCallback)
    ) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}

/**
 * @description Validates logins to include all needed params. It logs mistakes.
 * @return {boolean} - if the data is valid, it returns true.
 */
function validateLogin(req, next) {
    try {
        assert(
            typeof req.body.email === "string",
            "email is not a string or missing."
        );
        assert(
            typeof req.body.password === "string",
            "password is not a string or missing."
        );
    } catch (error) {
        console.log("User data is invalid: ", error.message);
        next({ message: error.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        next({ message: returnMessage, status: 400 });
    }

    if (
        inputChecks.emailCheck(req.body.email, errorCallback) &&
        inputChecks.passwordCheck(req.body.password, errorCallback)
    ) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}

class AuthenticationController {
    /**
     * @description Returns info about the server
     * @return {object} - returns a JSON object with some info about the server
     */
    info(req, res) {
            logger.info(`[AuthenticationController] add`);
            res.json(info);
        }
        /**
         * @description Tries to log in a user. Calls validateLogin
         * @todo get DAO connected
         * @return {object} - returns a JSON object with some info about the server
         */
    async login(req, res, next) {
            logger.info(`[AuthenticationController] login`);
            if (validateLogin(req, next)) {
                let dbCall = {};
                try {
                    dbCall = await authentication.readPasswordWhereEmail(req.body.email);
                } catch (error) {
                    logger.error(error);
                    return next(error);
                }
                logger.debug(dbCall.results);
                bcrypt
                    .compare(req.body.password, dbCall.results[0].password)
                    .then((match) => {
                        if (match) {
                            const payload = {
                                id: dbCall.results[0].id,
                            };
                            logger.info("passwords DID match, sending valid token");
                            // Create an object containing the data we want in the payload.
                            // Userinfo returned to the caller.
                            dbCall.results[0].token = jwt.sign(payload, jwtSecretKey, {
                                expiresIn: "2h",
                            });
                            delete dbCall.results[0].password;
                            logger.debug("Logged in, sending: ", dbCall);
                            return next(dbCall);
                        } else {
                            logger.debug("Wrong username or password.");
                            return next({
                                message: "Wrong username or password.",
                                errCode: 400,
                            });
                        }
                    });
            }
        }
        /**
         * @description Registers user. First checks if the user already exists and then puts them in the database with passwords encrypted
         * @todo seperate database calls into DAO
         * @param {*} req
         * @param {*} res
         * @param {*} next
         */
    async register(req, res, next) {
            logger.info(`[AuthenticationController] register`);
            logger.info(req.body);
            if (validateRegister(req, next)) {
                bcrypt.hash(req.body.password, 10, async(error, hash) => {
                    if (error) {
                        logger.error(error);
                        next({ message: `An error occured`, status: 500 });
                    } else if (hash) {
                        logger.debug("Password has been hashed");
                        req.body.hash = hash;
                        let dbCall = await authentication.create(req.body);
                        if (dbCall.userCreated === true) {
                            const payload = {
                                id: dbCall.returnedJSON.results[0].id,
                            };
                            dbCall.returnedJSON.results[0].token = jwt.sign(
                                payload,
                                jwtSecretKey, { expiresIn: "2h" }
                            );
                            next(dbCall.returnedJSON);
                        } else {
                            logger.error(
                                "An error occured while creating a new user: ",
                                dbCall.returnedJSON
                            );
                            next(dbCall.returnedJSON);
                        }
                    } else {
                        logger.error("Unknown error occured");
                        next({ message: `An unknown error has occured.`, status: 500 });
                    }
                });
            }
        }
        /**
         * @description Validates tokens. Called by the @see {@link login} function
         * @param {*} req
         * @param {*} res
         * @param {*} next
         */
    validateToken(req, res, next) {
        logger.info(`[AuthenticationController] validateToken`);
        // The headers should contain the authorization-field with value 'Bearer [token]'
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            logger.warn("Authorization header missing!");
            next({ message: "Authorization header is missing!", status: 401 });
        } else {
            // Strip the word 'Bearer ' from the headervalue
            const token = authHeader.substring(7, authHeader.length);

            jwt.verify(token, jwtSecretKey, (err, payload) => {
                if (err) {
                    logger.warn("Not authorized");
                    logger.debug(err);
                    next({
                        message: "The user is not authorised or the token is expired.",
                        status: 401,
                    });
                }
                if (payload) {
                    logger.debug("Token is valid", payload);
                    // User heeft toegang. Voeg UserId uit payload toe aan
                    // request, voor ieder volgend endpoint.
                    req.userId = payload.id;
                    next();
                }
            });
        }
    }
}

module.exports = new AuthenticationController();