const meal = require("../services/mealService");
const inputChecks = require("../inputChecks.js");
const assert = require("assert");
const logger = require("../config/config").logger;

function mealChecker(body, next) {
    try {
        assert(typeof body.name === "string", "name is not a string or missing.");
        assert(
            typeof body.description === "string",
            "description is not a string or missing."
        );
        assert(
            typeof body.createdOn === "string",
            "createdOn is not a string or missing."
        );
        assert(
            typeof body.offeredOn === "string",
            "offeredOn is not a string or missing."
        );
        assert(typeof body.price === "number", "price is not a number or missing.");
        assert(
            typeof body.allergyInfo === "string",
            "allergyInfo is not a string or missing."
        );
        assert(
            typeof body.ingredients === "object",
            "ingredients is not an object (list) or missing."
        );
        assert(
            typeof body.userId === "number",
            "userId is not a number or missing."
        );
        // assert(typeof body.studenthomeId === "number", "studenthomeId is not a number or missing.")
        assert(
            typeof body.maxParticipants === "number",
            "maxParticipants is not a number or missing."
        );
    } catch (err) {
        console.log("Mealdata is invalid: ", err.message);
        next({ message: err.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        next({ message: returnMessage, status: 400 });
    }

    if (
        inputChecks.dateCheck(body.createdOn, errorCallback) &&
        inputChecks.dateCheck(body.offeredOn, errorCallback)
    ) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}

class MealController {
    async add(req, res, next) {
        //TODO endpoint does not exist or smthng
        logger.info(`[MealController] add`);
        let data = {
            studenthomeId: parseInt(req.params.homeId),
            userId: req.userId,
            ...req.body,
        };
        //TODO: Does not work!?
        if (mealChecker(data, next)) {
            next(await meal.create(data));
        }
    }
    async remove(req, res, next) {
        logger.info(`[MealController] remove`);
        let existenceCheck = await meal.readWhereId(parseInt(req.params.mealId));
        if (existenceCheck.results) {
            let ownerCheck = await meal.readWhereUserIdAndMealId(
                parseInt(req.userId),
                parseInt(req.params.mealId)
            );
            if (ownerCheck.userIsOwner) {
                logger.debug("User is owner.");
                console.log(req.body, req.userId);
                return next(
                    await meal.remove(parseInt(req.params.mealId), parseInt(req.userId))
                );
            } else if (ownerCheck.error) {
                return next(ownerCheck.error);
            }
            logger.debug("User is not the owner.");
            return next({
                message: "User is not the owner of this meal.",
                status: 401,
            });
        } else {
            return next(existenceCheck);
        }
    }
    async getWhereId(req, res, next) {
        logger.info(`[MealController] getWhereId`);
        next(await meal.readWhereId(parseInt(req.params.mealId)));
    }
    async get(req, res, next) {
        logger.info(`[MealController] get`);
        logger.debug(req.query);
        next(await meal.readAllWhereStudentHome(req.params.homeId));
    }

    async update(req, res, next) {
        logger.info(`[MealController] update`);
        if (mealChecker(req.body, next)) {
            let data = {
                studenthomeId: req.params.homeId,
                ...req.body,
            };
            let params = {
                mealId: parseInt(req.params.mealId),
                userId: parseInt(req.userId),
            };
            let existenceCheck = await meal.readWhereId(params.mealId);
            if (existenceCheck.results) {
                let ownerCheck = await meal.readWhereUserIdAndMealId(
                    parseInt(params.userId),
                    parseInt(params.mealId)
                );
                if (ownerCheck.userIsOwner) {
                    if (ownerCheck.results) {
                        logger.debug("User is owner");
                        console.debug("params: ", params);
                        next(
                            await meal.update(
                                data,
                                parseInt(params.mealId),
                                parseInt(params.userId)
                            )
                        );
                    }
                } else if (ownerCheck.error) {
                    next(ownerCheck.error);
                    // } else{
                    //     next({message: "User does not own any meals"})
                }
                logger.debug("User is not the owner.");
                next({ message: "User is not the owner of this meal.", status: 401 });
            } else {
                logger.debug("Meal does not exist (or error).");
                next(existenceCheck);
            }
        }
    }
}
module.exports = new MealController();