const assert = require("assert");
const inputChecks = require("../inputChecks");
const logger = require("../config/config").logger;
const home = require("../services/homeService");

function studentHomeChecker(body, next) {
    try {
        assert(typeof body.name === "string", "name is not a string or missing.");
        assert(
            typeof body.address === "string",
            "address is not a string or missing."
        );
        assert(
            typeof body.houseNumber === "number",
            "houseNumber is not a number or missing."
        );
        assert(typeof body.city === "string", "city is not a string or missing.");
        assert(
            typeof body.phoneNumber === "string",
            "phoneNumber is not a string or missing."
        );
        assert(
            typeof body.postalCode === "string",
            "postalCode is not a string or missing."
        );
    } catch (err) {
        console.log("Studenthomedata is invalid: ", err.message);
        next({ message: err.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        next({ message: returnMessage, status: 400 });
    }

    if (
        inputChecks.phoneCheck(body.phoneNumber, errorCallback) &&
        inputChecks.postalCodeCheck(body.postalCode, errorCallback)
    ) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}

function updateStudentHomeChecker(body, next) {
    try {
        assert(typeof body.name === "string", "name is not a string or missing.");
        assert(
            typeof body.address === "string",
            "address is not a string or missing."
        );
        assert(
            typeof body.houseNumber === "number",
            "houseNumber is not a number or missing."
        );
        assert(typeof body.city === "string", "city is not a string or missing.");
        assert(
            typeof body.phoneNumber === "string",
            "phoneNumber is not a string or missing."
        );
        assert(
            typeof body.postalCode === "string",
            "postalCode is not a string or missing."
        );
        assert(
            typeof body.userId === "number",
            "userId is not a number or missing."
        );
    } catch (err) {
        console.log("Studenthomedata is invalid: ", err.message);
        next({ message: err.message, status: 400 });
        return false;
    }

    function errorCallback(returnMessage) {
        next({ message: returnMessage, status: 400 });
    }

    if (
        inputChecks.phoneCheck(body.phoneNumber, errorCallback) &&
        inputChecks.postalCodeCheck(body.postalCode, errorCallback)
    ) {
        logger.debug("Data is valid");
        return true;
    } else {
        return false;
    }
}
class StudentHomeController {
    async add(req, res, next) {
        logger.info(`[StudentHomeController] add`);
        if (studentHomeChecker(req.body, next)) {
            let data = req.body;
            data.userId = req.userId;
            next(await home.create(data));
        }
    }

    async remove(req, res, next) {
        logger.info(`[StudentHomeController] remove`);
        let existenceCheck = await home.readWhereId(parseInt(req.params.homeId));
        if (existenceCheck.results) {
            let ownerCheck = await home.readWhereUserIdAndStudenthomeId(
                req.userId,
                parseInt(req.params.homeId)
            );
            if (ownerCheck.userIsOwner) {
                if (ownerCheck.results) {
                    logger.debug("User is owner.");
                    return next(
                        await home.remove(parseInt(req.params.homeId), parseInt(req.userId))
                    );
                }
            } else if (ownerCheck.error) {
                return next(ownerCheck.error);
            }
            logger.debug("User is not the owner.");
            return next({
                message: "User is not the owner of this studenthome.",
                status: 401,
            });
        } else {
            logger.debug("Home does not exist (or error).");
            next(existenceCheck);
        }
    }

    async get(req, res, next) {
        logger.info(`[StudentHomeController] get`);
        const name = req.query.name;
        const city = req.query.city;
        if (name || city) {
            next(await home.readWhereNameAndCity(name, city));
        } else if (req.params.homeId) {
            next(await home.readWhereId(parseInt(req.params.homeId)));
        } else {
            next({ message: "An unknown error occured.", status: 400 });
        }
    }

    async update(req, res, next) {
        logger.info(`[StudentHomeController] update`);
        if (updateStudentHomeChecker(req.body, next)) {
            let params = {
                homeId: parseInt(req.params.homeId),
                userId: parseInt(req.userId),
            };
            let existenceCheck = await home.readWhereId(req.params.homeId);
            if (existenceCheck.results) {
                let ownerCheck = await home.readWhereUserIdAndStudenthomeId(
                    params.userId,
                    params.homeId
                );
                if (ownerCheck.userIsOwner) {
                    if (ownerCheck.results) {
                        logger.debug("User is owner.");
                        console.debug("params: ", params);
                        next(
                            await home.update(
                                req.body,
                                parseInt(params.homeId),
                                parseInt(params.userId)
                            )
                        );
                    }
                } else if (ownerCheck.error) {
                    next(ownerCheck.error);
                }
                logger.debug("User is not the owner.");
                next({
                    message: "User is not the owner of this studentHome.",
                    status: 401,
                });
            } else {
                logger.debug("Home does not exist (or error).");
                next(existenceCheck);
            }
        }
    }
}

module.exports = new StudentHomeController();