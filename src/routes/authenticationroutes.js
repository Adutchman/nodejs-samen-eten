//
// Authentication routes
//
const authController = require("../controllers/authenticationcontroller");
const express = require("express");
const router = express.Router();

//UC-101 registreren
router.post("/register", authController.register);
//UC-102 login
router.post("/login", authController.login);

// router.get('/validate', authController.validateToken, authController.renewToken)
//UC-103 Systeeminfo opvragen
router.get("/info", authController.info);
module.exports = router;