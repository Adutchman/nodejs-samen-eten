const database = require("./databaseService");
const logger = require("../config/config").logger;
const genericQuery = require("../services/genericQueries");

class Authentication {
    _mapResults(results) {
        logger.debug("Mapping results.");
        return results.map((item) => {
            if (item.Password) {
                return {
                    id: item.ID,
                    firstName: item.First_Name,
                    lastName: item.Last_Name,
                    emailAdress: item.Email,
                    studentNumber: item.Student_Number,
                    password: item.Password,
                };
            } else {
                return {
                    id: item.ID,
                    firstName: item.First_Name,
                    lastName: item.Last_Name,
                    emailAdress: item.Email,
                    studentNumber: item.Student_Number,
                };
            }
        });
    }

    async readPasswordWhereEmail(userEmail) {
        logger.info(`[DB Home] create`);
        return new Promise(async(resolve, reject) => {
            let results = {};
            try {
                results = await genericQuery.readWhereSingle(
                    "SELECT * FROM `user` WHERE `Email` = ?", [userEmail],
                    "email address",
                    "user"
                );
                logger.debug("query: ", results);
            } catch (error) {
                logger.debug(error);
                return reject(error);
            }
            logger.debug("User is found with email.");
            return resolve({
                message: `The user with the email ${userEmail} has been logged in. The token is valid for 2 hours.`,
                results: this._mapResults(results),
                status: 200,
            });
        });
    }

    async create(user) {
        logger.info(`[DB Home] create`);
        let results = {};
        try {
            results = await database.execute(
                "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)", [
                    user.firstName,
                    user.lastName,
                    user.email,
                    user.studentNumber,
                    user.hash,
                ]
            );
        } catch (error) {
            if (error.code === "ER_DUP_ENTRY") {
                logger.info("Entry already exists");
                return {
                    returnedJSON: { message: "Entry already exists.", status: 400 },
                    userCreated: false,
                };
            } else {
                logger.info(error);
                return {
                    returnedJSON: { message: "Unspecified SQL Error.", status: 400 },
                    userCreated: false,
                };
            }
        }
        let returnedResults = {
            id: results.insertId,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            studentNumber: user.studentNumber,
        };
        logger.debug("User has been added to DB.");
        return {
            returnedJSON: {
                message: `The user ${user.firstName} has been added to the database.`,
                results: [returnedResults],
                status: 200,
            },
            userCreated: true,
        };
    }

    async readWhereId(userId) {
        logger.info(`[DB Home] readWhereId`);
        let results = {};
        try {
            results = await database.execute(
                "SELECT `ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number` FROM `user` WHERE `ID` = ?", [userId]
            );
        } catch (error) {
            logger.info(error);
            return {
                returnedJSON: { message: "Unspecified SQL Error.", status: 400 },
                userRead: false,
            };
        }
        logger.debug("User has been read.");
        return {
            returnedJSON: { message: `User has been found`, status: 200 },
            userRead: true,
        };
    }
}

module.exports = new Authentication();