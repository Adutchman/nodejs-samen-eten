const database = require("./databaseService");
const logger = require("../config/config").logger;
const genericQuery = require("./genericQueries");

class Meal {
    mapResults(results) {
        logger.debug("Mapping results.");
        return results.map((item) => {
            return {
                id: item.ID,
                name: item.Name,
                description: item.Description,
                ingredients: item.Ingredients,
                allergyInfo: item.Allergies,
                createdOn: item.CreatedOn,
                offeredOn: item.OfferedOn,
                price: item.Price,
                userId: item.UserID,
                maxParticipants: item.MaxParticipants,
                studenthomeId: item.StudenthomeID,
            };
        });
    }
    async create(meal) {
        logger.info(`[DB Meal] create`);
        let results = {};
        try {
            results = await genericQuery.create(
                "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`,`Price`, `UserID`, `StudenthomeID` , `MaxParticipants`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                    meal.name,
                    meal.description,
                    meal.ingredients.toString(),
                    meal.allergyInfo,
                    meal.createdOn,
                    meal.offeredOn,
                    meal.price,
                    meal.userId,
                    meal.studenthomeId,
                    meal.maxParticipants,
                ]
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        logger.debug("Meal added to DB.");
        return {
            message: `${
        meal.name.charAt(0).toUpperCase() + meal.name.slice(1)
      } has been added to the database with the id of ${results.insertId}`,
            results: [{
                id: results.insertId,
                name: meal.name,
                description: meal.description,
                ingredients: meal.ingredients.toString(),
                allergyInfo: meal.allergyInfo,
                createdOn: meal.createdOn,
                offeredOn: meal.offeredOn,
                price: meal.price,
                userId: meal.userId,
                studenthomeId: meal.studenthomeId,
                maxParticipants: meal.maxParticipants,
            }, ],
            status: 200,
        };
    }

    /**
     * @async
     * @description queries DB with a SELECT query with a specific ID
     * @param {number} homeId
     */
    async readWhereId(mealId) {
        logger.info(`[DB Meal] readWhereId`);
        let results = {};
        try {
            results = await genericQuery.readWhereSingle(
                "SELECT * FROM meal WHERE ID = ?", [mealId],
                "id",
                "meal"
            );
        } catch (error) {
            logger.debug(error);
            return error;
        }
        logger.debug("Meal has beend found.");
        return {
            message: "Meal successfully retrieved.",
            results: this.mapResults(results),
            status: 200,
        };
    }

    async remove(mealId, userId) {
        logger.info(`[DB Meal] remove`);
        let results = {};
        try {
            results = await genericQuery.remove(
                "DELETE FROM meal WHERE ID = ? AND UserID = ?", [mealId, userId]
            );
        } catch (error) {
            logger.debug(error);
            return error;
        }
        logger.debug("Meal was deleted");
        return { message: `Meal has been successfully deleted.`, status: 200 };
    }

    async update(meal, mealId, userId) {
        logger.info(`[DB Meal] update`);
        let results = {};
        try {
            results = await genericQuery.update(
                "UPDATE meal SET Name = ?, Description = ?, Ingredients = ?, Allergies = ?, CreatedOn = ?, OfferedOn = ?, Price = ?, UserId = ?, StudenthomeID = ?, MaxParticipants = ? WHERE ID = ? AND UserID = ?", [
                    meal.name,
                    meal.description,
                    meal.ingredients.toString(),
                    meal.allergyInfo,
                    meal.createdOn,
                    meal.offeredOn,
                    meal.price,
                    meal.userId,
                    meal.studenthomeId,
                    meal.maxParticipants,
                    mealId,
                    userId,
                ]
            );
        } catch (error) {
            logger.debug(error);
            return error;
        }
        logger.debug("Meal was updated");
        return {
            message: `Meal has been successfully updated.`,
            results: [{
                id: mealId,
                name: meal.name,
                description: meal.description,
                ingredients: meal.ingredients.toString(),
                allergyInfo: meal.allergyInfo,
                createdOn: meal.createdOn,
                offeredOn: meal.offeredOn,
                price: meal.price,
                userId: meal.userId,
                studenthomeId: meal.studenthomeId,
                maxParticipants: meal.maxParticipants,
            }, ],
            status: 200,
        };
    }
    async readWhereUserIdAndMealId(userId, mealId) {
        logger.info(`[DB Meal] readWhereUserIdAndMealId`);
        let results = {};
        try {
            results = await genericQuery.readWhereMultiple(
                "SELECT * FROM meal WHERE UserID = ? AND ID = ?", [userId, mealId],
                "meals"
            );
        } catch (error) {
            logger.error(error);
            if (error === genericQuery.UNKNOWN_SQL_ERROR) {
                return { error: error };
            }
            return { userIsOwner: false };
        }
        return { results: results, userIsOwner: true };
    }

    async readAllWhereStudentHome(studentHomeId) {
        logger.info(`[DB Meal] readAllWhereStudentHome`);
        let results = {};
        try {
            results = await genericQuery.readWhereMultiple(
                "SELECT * FROM meal WHERE StudentHomeID = ?", [studentHomeId],
                "meals"
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        logger.debug("Meal has been found");
        return {
            message: "Meal successfully retrieved.",
            results: this.mapResults(results),
            status: 200,
        };
    }
}

module.exports = new Meal();