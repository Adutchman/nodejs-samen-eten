const database = require("./databaseService");
const logger = require("../config/config").logger;
const genericQuery = require("./genericQueries");

class Home {
    mapResultsParticipants(results) {
        logger.debug("Mapping items");
        return results.map((item) => {
            return {
                userId: item.UserID,
                studenthomeId: item.StudenthomeID,
                mealId: item.MealID,
                SignedUpOn: item.SignedUpOn,
            };
        });
    }
    mapResultsParticipantsView(results) {
        logger.debug("Mapping items");
        return results.map((item) => {
            return {
                userId: item.UserID,
                firstName: item.First_Name,
                lastName: item.Last_Name,
                email: item.Email,
                studentNumber: item.Student_Number,
                studenthomeId: item.StudenthomeID,
                mealId: item.MealID,
                mealName: item.Name,
                mealMaxParticipants: item.MaxParticipants,
            };
        });
    }
    mapResultsUsers(results) {
            logger.debug("Mapping items");
            return results.map((item) => {
                return {
                    id: item.ID,
                    firstName: item.First_Name,
                    lastName: item.Last_Name,
                    email: item.Email,
                    studentNumber: item.Student_Number,
                };
            });
        }
        /**
         * @todo Not yet finished, needs meal readWhereId
         * @param {object} participant
         * @returns {object} JSON response
         */
    async create(participant) {
        logger.info(`[DB Participant] create`);
        let query = {};
        try {
            query = await genericQuery.create(
                "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, ?)", [
                    participant.userId,
                    participant.studenthomeId,
                    participant.mealId,
                    participant.signedUpOn,
                ]
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        logger.debug("Participant added to DB.");
        return {
            message: `The participant has been added to the database.`,
            results: [participant],
            status: 200,
        };
    }
    async remove(participantId, mealId) {
        logger.info(`[DB Participant] remove`);
        try {
            await genericQuery.remove(
                "DELETE FROM participants WHERE UserID = ? AND MealID = ?", [participantId, mealId]
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        return {
            message: `Participant has been successfully deleted.`,
            status: 200,
        };
    }

    async readWhereId(participantId) {
        logger.info(`[DB Participant] readWhereId`);
        let results = {};
        try {
            results = await genericQuery.readWhereSingle(
                "SELECT ID,First_Name,Last_Name,Email,Student_Number FROM user WHERE ID = ?", [participantId],
                "id",
                "participant"
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        return {
            message: "Participants successfully retrieved.",
            results: this.mapResultsParticipantsView(results),
            status: 200,
        };
    }
    async readAllWhereMealId(mealId) {
        logger.info(`[DB Meal] readAllWhereMealId`);
        let results = {};
        try {
            results = await genericQuery.readWhereMultiple(
                "SELECT * FROM view_participants WHERE MealID = ?", [mealId],
                "participants"
            );
        } catch (error) {
            logger.error(error);
            return error;
        }
        return {
            message: "Participants successfully retrieved.",
            results: this.mapResultsParticipantsView(results),
            status: 200,
        };
    }
}

module.exports = new Home();