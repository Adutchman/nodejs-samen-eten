process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../src/index");
const pool = require("../src/config/database");
const logger = require("../src/config/config").logger;
const key = require("../src/config/config").jwtSecretKey;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const assert = require("assert");

chai.should();
chai.use(chaiHttp);

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);

const INSERT_USER =
    "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    '(1, "first", "last", "name@server.nl","1234567", "Anyoldpassword1!"),' +
    '(2, "Jan", "Janssen", "jan@janssen.nl","1234567", "Anyoldpassword1!");';

const CLEAR_DB = "DELETE FROM `user`";

describe("Authentication", () => {
    // Add user to test TC-101-4 for duplicates
    before((done) => {
        logger.info("User tabel clear");
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                logger.error(`beforeEach CLEAR error: ${err}`);
            }
        });
        bcrypt.hash("Anyoldpassword1!", 10).then((hash) => {
            const ADD_USER =
                "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES('1', 'Jan', 'Smit', 'jsmit@server.nl','222222', ?),('2', 'Mark', 'Gerrits', 'mark@gerrits.nl', '333333', ?)";
            pool.query(ADD_USER, [hash, hash], (err, rows, fields) => {
                logger.warn(rows);
                logger.info("Add user");
                if (err) {
                    logger.error(`beforeCLEAR error: ${err}`);
                    done(err);
                } else {
                    done();
                }
            });
        });
    });

    // After successful register we have a valid token. We export this token
    // for usage in other testcases that require login.

    describe("UC101 Registation", () => {
        it("TC-101-1 should throw an error when no firstName is provided", (done) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    //No firstName
                    lastName: "OtherLastName ",
                    email: "other@test.nl",
                    password: "Anyoldpassword1!",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(400);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");

                    res.body.error.should.be
                        .a("string")
                        .that.equals("firstName is not a string or missing.");

                    done();
                });
        });
    });
    it("TC-101-2 should return a valid error when an invalid email adress is entered", (done) => {
        chai
            .request(server)
            .post("/api/register")
            .send({
                firstName: "FirstName",
                lastName: "LastName",
                email: "@@asdfI*",
                studentNumber: 1234567,
                password: "Anyoldpassword1!",
            })
            .end((err, res) => {
                assert.ifError(err);
                res.should.have.status(400);
                res.should.be.an("object");

                res.body.should.be.an("object").that.has.all.keys("error");
                res.body.error.should.be.a("string").that.equals("Email is invalid.");
                done();
            });
    });
    it("TC-101-3 should return a valid error when an invalid password is entered", (done) => {
        chai
            .request(server)
            .post("/api/register")
            .send({
                firstName: "FirstName",
                lastName: "LastName",
                email: "test@test.nl",
                studentNumber: 1234567,
                password: "secret",
                //Minimum eight characters, at least one upper case English letter, one lower case English letter, one number and one special character
            })
            .end((err, res) => {
                assert.ifError(err);
                res.should.have.status(400);
                res.should.be.an("object");

                res.body.should.be.an("object").that.has.all.keys("error");
                res.body.error.should.be
                    .a("string")
                    .that.equals(
                        "Password is invalid, It should have a minimum of eight characters, at least one upper case English letter, one lower case English letter, one number and one special character."
                    );
                done();
            });
    });
    it("TC-101-4 should return a valid error when the user already exists", (done) => {
        pool.query(INSERT_USER, (error, result) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    firstName: "Jan",
                    lastName: "Smit",
                    email: "jsmit@server.nl",
                    studentNumber: 222222,
                    password: "Anyoldpassword1!",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(400);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");
                    res.body.error.should.be
                        .a("string")
                        .that.equals("Entry already exists.");
                    done();
                });
        });
    });
    it("TC-101-5 should return a token when providing valid information", (done) => {
        chai
            .request(server)
            .post("/api/register")
            .send({
                firstName: "FirstName",
                lastName: "LastName",
                email: "test@test.nl",
                studentNumber: 1234567,
                password: "Anyoldpassword1!",
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a("object");
                const response = res.body;
                response.results[0].should.have.property("token").which.is.a("string");
                done();
            });
    });

    describe("UC102 Login", () => {
        /**
         * This assumes that a user with given credentials exists. That is the case
         * when register has been done before login.
         */
        it("TC-102-1 should return a valid error when a required field is missing", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "apskdflkjlkj",
                    //password missing
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(400);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");
                    res.body.error.should.be
                        .a("string")
                        .that.equals("password is not a string or missing.");
                    done();
                });
        });
        it("TC-102-2 should return a valid error when an invalid email adress is entered", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "apskdflkjlkj",
                    password: "Anyoldpassword1!",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(400);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");
                    res.body.error.should.be.a("string").that.equals("Email is invalid.");
                    done();
                });
        });
        it("TC-102-3 should return a valid error when an invalid password is entered", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "name@server.nl",
                    password: "secret",
                    //Minimum eight characters, at least one upper case English letter, one lower case English letter, one number and one special character
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(400);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");
                    res.body.error.should.be
                        .a("string")
                        .that.equals(
                            "Password is invalid, It should have a minimum of eight characters, at least one upper case English letter, one lower case English letter, one number and one special character."
                        );
                    done();
                });
        });
        it("TC-101-4 should return a valid error when the user does not exist", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "adsfasf@asdfasd.nl",
                    password: "Anyoldpassword1!",
                })
                .end((err, res) => {
                    assert.ifError(err);
                    res.should.have.status(404);
                    res.should.be.an("object");

                    res.body.should.be.an("object").that.has.all.keys("error");
                    res.body.error.should.be
                        .a("string")
                        .that.equals(
                            "There is no user with the email address: adsfasf@asdfasd.nl."
                        );
                    done();
                });
        });

        it("TC-102-5 should return a token when providing valid information", (done) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    firstName: "FirstName",
                    lastName: "LastName",
                    email: "name@server.nl",
                    studentNumber: 1234567,
                    password: "Anyoldpassword1!",
                })
                .end((err, res) => {
                    chai
                        .request(server)
                        .post("/api/login")
                        .send({
                            email: "name@server.nl",
                            password: "Anyoldpassword1!",
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a("object");
                            const response = res.body;
                            response.results[0].should.have
                                .property("token")
                                .which.is.a("string");

                            response.results[0].should.have
                                .property("firstName")
                                .which.is.a("string");

                            response.results[0].should.have
                                .property("lastName")
                                .which.is.a("string");
                            done();
                        });
                });
        });
    });
});