process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
process.env.NODE_ENV = "testing";
process.env.LOGLEVEL = "error";

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../src/index");
const pool = require("../src/config/database");
const logger = require("../src/config/config").logger;
const key = require("../src/config/config").jwtSecretKey;
const jwt = require("jsonwebtoken");
const assert = require("assert");

chai.should();
chai.use(chaiHttp);

logger.debug(`Running tests using database '${process.env.DB_DATABASE}'`);

/**
 * Query om alle tabellen leeg te maken, zodat je iedere testcase met
 * een schone lei begint. Let op, ivm de foreign keys is de volgorde belangrijk.
 *
 * Let ook op dat je in de dbconfig de optie multipleStatements: true toevoegt!
 */
const CLEAR_STUDENTHOME = "DELETE IGNORE FROM `studenthome`;";

const CLEAR_DB =
    "DELETE IGNORE FROM `studenthome`; DELETE IGNORE FROM `meal`; DELETE IGNORE FROM `participants`; DELETE IGNORE FROM `user`";
const INSERT_STUDENTHOMES =
    "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserId`, `Postal_Code`, `Telephone`, `City`) VALUES " +
    "(1, 'Avans Lovensdijkstraat', 'Lovensdijkstraat', 61, 1, '4818AJ', '+3112345678', 'Breda')," +
    "(2, 'Avans Hogeschoollaan', 'Hogeschoollaan', 1, 1, '4818CR', '+3112345678', 'Breda')," +
    "(3, 'Fontys', 'Prof. Goossenslaan', 1, 2, '5022DM', '+3112345678', 'Tilburg');";

const INSERT_USER =
    "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES" +
    '(1, "first", "last", "name@server.nl","1234567", "Anyoldpassword1!"),' +
    '(2, "Jan", "Janssen", "jan@janssen.nl","1234567", "Anyoldpassword1!");';

describe("Manage studenthome", () => {
    let token;
    before((done) => {
        logger.info("studenthome tabel clear");
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEAR error: ${err}`);
                done(err);
            } else {
                done();
            }
        });
    });
    before((done) => {
        logger.info("studenthomes inserted");
        pool.query(INSERT_USER, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEAR error: ${err}`);
                done(err);
            } else {
                done();
            }
        });
    });

    before((done) => {
        jwt.sign({ id: 1 }, key, { expiresIn: "2h" }, (err, jwtToken) => {
            token = jwtToken;
        });
        logger.info("studenthomes inserted");
        pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
            if (err) {
                logger.error(`before CLEAR error: ${err}`);
                done(err);
            } else {
                done();
            }
        });
    });
    /**
     * Query om twee movies toe te voegen. Let op de UserId, die moet matchen
     * met de user die je ook toevoegt.
     */

    describe("StudentHome", function() {
        describe("create, UC-201", function() {
            it("TC-201-1 should return valid error when required value is not present", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome")
                    .set("Authorization", "Bearer " + token)
                    .send({
                        //Name is missing
                        address: "someotherStreet",
                        houseNumber: 8,
                        postalCode: "1234AB",
                        city: "Tilburg",
                        phoneNumber: "+3112345678",
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("name is not a string or missing.");
                        done();
                    });
            });
            it("TC-201-2 should return valid error when postal code is invalid", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "someName",
                        address: "someotherStreet",
                        houseNumber: 8,
                        postalCode: "124AB",
                        city: "Tilburg",
                        phoneNumber: "+3112345678",
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("postalCode is invalid.");
                        done();
                    });
            });
            it("TC-201-3 should return valid error when phone number is invalid", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "someName",
                        address: "someotherStreet",
                        houseNumber: 8,
                        postalCode: "1245AB",
                        city: "Tilburg",
                        phoneNumber: "+3134578",
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("phoneNumber is invalid.");
                        done();
                    });
            });
            it("TC-201-5 should return valid response when user is not logged in", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .send({
                        name: "someName",
                        address: "someotherStreet",
                        houseNumber: 8,
                        postalCode: "1245AB",
                        city: "Tilburg",
                        phoneNumber: "+3112345678",
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });
            it("TC-201-6 should return valid response when studenthome is succesfully added", (done) => {
                chai
                    .request(server)
                    .post("/api/studenthome")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "someName",
                        address: "someotherStreet",
                        houseNumber: 8,
                        postalCode: "1245AB",
                        city: "Tilburg",
                        phoneNumber: "+3112345678",
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.should.be.an("object");
                        res.body.should.be
                            .an("object")
                            .that.has.all.keys("message", "results");
                        res.body.results[0].should.be
                            .an("object")
                            .that.has.all.keys(
                                "id",
                                "name",
                                "address",
                                "houseNumber",
                                "userId",
                                "postalCode",
                                "phoneNumber",
                                "city"
                            );
                        res.body.message.should.be
                            .a("string")
                            .that.include(
                                "The someName home has been added to the database with the id: "
                            );
                        res.body.results[0].should.include({
                            name: "someName",
                            address: "someotherStreet",
                            houseNumber: 8,
                            postalCode: "1245AB",
                            city: "Tilburg",
                            phoneNumber: "+3112345678",
                        });
                        done();
                    });
            });
        });

        describe("get, UC-202", function() {
            //TODO hoe werkt dit?
            it("TC-202-1 should return error when there are 0 items", (done) => {
                chai
                    .request(server)
                    .get("/api/studenthome?city=alksdfjlkasdjflkasdjf;lksdjfls;kfj")
                    .set("authorization", "Bearer " + token)
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "No studenthomes where found in alksdfjlkasdjflkasdjf;lksdjfls;kfj."
                            );
                        done();
                    });
            });

            it("TC-202-2 should return 2 items when there are 2 results", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome?city=Breda")
                        .end((err, res) => {
                            logger.debug(res.body);
                            res.should.have.status(200);
                            res.body.results.should.be.an("array").that.has.lengthOf(2);
                            res.body.should.be
                                .an("object")
                                .that.has.all.keys("message", "results");
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "id",
                                    "name",
                                    "address",
                                    "houseNumber",
                                    "userId",
                                    "postalCode",
                                    "phoneNumber",
                                    "city"
                                );
                            done();
                        });
                });
            });

            it("TC-202-3 should return error when searching on non-existant city", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome?city=Brussel")
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("No studenthomes where found in Brussel.");
                            done();
                        });
                });
            });
            it("TC-202-4 should return error when searching on non-existant studenthome name", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome?name=Epsilon")
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals(
                                    "No studenthomes where found with the name Epsilon."
                                );
                            done();
                        });
                });
            });

            it("TC-202-5 should return items when searching for an existant city", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome?city=Breda")
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.results.should.be.an("Array").that.has.length(2);
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "id",
                                    "name",
                                    "address",
                                    "houseNumber",
                                    "userId",
                                    "postalCode",
                                    "phoneNumber",
                                    "city"
                                );
                            done();
                        });
                });
            });
            it("TC-202-5 should return items when searching for an existant name", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome?name=Avans Hogeschoollaan")
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.results.should.be.an("Array").that.has.length(1);
                            res.body.results[0].should.be
                                .an("object")
                                .that.has.all.keys(
                                    "id",
                                    "name",
                                    "address",
                                    "houseNumber",
                                    "userId",
                                    "postalCode",
                                    "phoneNumber",
                                    "city"
                                );
                            done();
                        });
                });
            });
        });
        describe("get, UC-203", function() {
            it("TC-203-1 should give error when requesting non existent studenthome ID", (done) => {
                chai
                    .request(server)
                    .get("/api/studenthome/10")
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("There is no home with the id: 10.");
                        done();
                    });
            });
            it("TC-203-2 should return valid response when studenthome is found", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .get("/api/studenthome/1")
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.results[0].should.eql({
                                id: 1,
                                name: "Avans Lovensdijkstraat",
                                address: "Lovensdijkstraat",
                                houseNumber: 61,
                                userId: 1,
                                postalCode: "4818AJ",
                                phoneNumber: "+3112345678",
                                city: "Breda",
                            });
                            done();
                        });
                });
            });
        });

        describe("update, UC-204", function() {
            after((done) => {
                logger.info("studenthome tabel clear");
                pool.query(CLEAR_STUDENTHOME, (err, rows, fields) => {
                    if (err) {
                        logger.error(`before CLEAR error: ${err}`);
                        done(err);
                    } else {
                        done();
                    }
                });
            });

            after((done) => {
                logger.info("studenthomes inserted");
                pool.query(INSERT_STUDENTHOMES, (err, rows, fields) => {
                    if (err) {
                        logger.error(`before CLEAR error: ${err}`);
                        done(err);
                    } else {
                        done();
                    }
                });
            });

            it("UC-204-1 should return valid error when required value is not present", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/1")
                    .set("Authorization", "Bearer " + token)
                    .send({
                        //Name missing
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "2345AB",
                        city: "Antwerpen",
                        phoneNumber: "+311234578",
                        userId: 1,
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("name is not a string or missing.");
                        done();
                    });
            });
            it("TC-204-2 should return valid error when postal code is invalid", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/1")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "A new home",
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "235AB",
                        city: "Antwerpen",
                        phoneNumber: "+3112345678",
                        userId: 1,
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("postalCode is invalid.");
                        done();
                    });
            });
            it("TC-204-3 should return valid error when phone number is invalid", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/1")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "A new home",
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "2345AB",
                        city: "Antwerpen",
                        phoneNumber: "+3134578",
                        userId: 1,
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(400);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("phoneNumber is invalid.");
                        done();
                    });
            });
            it("TC-204-4 should return valid error when the studenthome does not exist", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/10")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "A new home",
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "2345AB",
                        city: "Antwerpen",
                        phoneNumber: "+3112345678",
                        userId: 1,
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(404);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals("There is no home with the id: 10.");
                        done();
                    });
            });
            it("TC-204-5 should return valid response when user is not logged in", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/1")
                    .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                    .send({
                        name: "A new home",
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "2345AB",
                        city: "Antwerpen",
                        phoneNumber: "+3112345678",
                        userId: 1,
                    })
                    .end((err, res) => {
                        assert.ifError(err);
                        res.should.have.status(401);
                        res.should.be.an("object");

                        res.body.should.be.an("object").that.has.all.keys("error");
                        res.body.error.should.be
                            .a("string")
                            .that.equals(
                                "The user is not authorised or the token is expired."
                            );
                        done();
                    });
            });
            it("TC-204-6 should return valid response when studenthome is succesfully updated", (done) => {
                chai
                    .request(server)
                    .put("/api/studenthome/1")
                    .set("authorization", "Bearer " + token)
                    .send({
                        name: "A new home",
                        address: "Eksterstraat",
                        houseNumber: 8,
                        postalCode: "2345AB",
                        city: "Antwerpen",
                        phoneNumber: "+3112345678",
                        userId: 1,
                    })
                    .end((err, res) => {
                        console.log("!-!");
                        res.should.have.status(200);
                        res.should.be.an("object");
                        res.body.should.be
                            .an("object")
                            .that.has.all.keys("message", "results");
                        res.body.message.should.be
                            .a("string")
                            .that.equals("Studenthome has been successfully updated.");
                        res.body.results[0].should.be
                            .an("object")
                            .that.has.all.keys(
                                "id",
                                "name",
                                "address",
                                "houseNumber",
                                "userId",
                                "postalCode",
                                "phoneNumber",
                                "city"
                            );
                        done();
                    });
            });
        });

        describe("delete, UC-205", function() {
            it("TC-205-1 should return valid error when studenthome is not found", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/10")
                        .set("authorization", "Bearer " + token)
                        .end((err, res) => {
                            assert.ifError(err);
                            res.should.have.status(404);
                            res.should.be.an("object");
                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("There is no home with the id: 10.");
                            done();
                        });
                });
            });

            it("TC-205-2 should return valid error when user is not logged in", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/1")
                        .set("authorization", "Bearer lkjasdfjlkadsjf;lk")
                        .end((err, res) => {
                            let { message, error } = res.body;
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals(
                                    "The user is not authorised or the token is expired."
                                );
                            done();
                        });
                });
            });

            it("TC-205-3 should return valid error when user is not the owner", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/3")
                        .set("authorization", "Bearer " + token)
                        .end((err, res) => {
                            console.log("-- ", res.body);
                            let { message, error } = res.body;
                            assert.ifError(err);
                            res.should.have.status(401);
                            res.should.be.an("object");

                            res.body.should.be.an("object").that.has.all.keys("error");
                            res.body.error.should.be
                                .a("string")
                                .that.equals("User is not the owner of this studenthome.");
                            done();
                        });
                });
            });

            it("TC-205-4 should return valid response when studenthome is succesfully deleted", (done) => {
                pool.query(INSERT_STUDENTHOMES, (error, result) => {
                    chai
                        .request(server)
                        .delete("/api/studenthome/1")
                        .set("authorization", "Bearer " + token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.should.be.an("object");
                            res.body.should.eql({
                                message: "Studenthome has been successfully deleted.",
                            });
                            done();
                        });
                });
            });
        });
    });
});